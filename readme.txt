DESCRIPTION
This module enables users to vote on nodes and allows community promotion of nodes to the Drupal home page. The list of possible votes and the value of each vote is configured by the administrator.

Each node maintains a score, based on the sum of all votes. As each vote is processed, the score is recalculated and if necessary, the post is either promoted to home page or unpublished. The thresholds for these actions are defined by the administrator. (For Drupal 4.7 and later, Node Moderation depends on the VotingAPI module.)

USAGE
1. Copy the nmoderation and votingapi directories to the Drupal modules/ directory.

2. Enable boht VotingAPI and the Node Moderation module in the "administer >> modules" menu.

3. Set your desired permissions to allow certain user roles access to node moderation. In the menu "administer >> users" and select the "configure >> permissions" tab.  Look at the permissions associated with the nmoderation module, they are:

--- administer node moderation configuration: This permission is for admins only, as it allows users to setup and manipulate the vote matrix.

--- administer node moderation votes: This permission is for admins or power users, it allows users to manage vote by either editing and deleting other users votes.

--- nmoderate nodes: This permission is for any user roles that you want to be allowed to vote on nodes.  NOTE: As a general rule anonymous users should not have this permission.
   
4. Setup Moderation Votes.  The first step is to create moderation labels which allow users to rate a node. Go to "administer >> node moderation >> moderation votes" menu. In the vote field, enter the textual labels which users will see when casting their votes. Some examples are:

    * Excellent Post! +2
    * Good Post! +1
    * Keep in the Blogs Section. 0
    * Not very good -1
    * Dump It! -2

So that users know how their votes affect the node, these examples include the vote value as part of the label, although that is optional. Using the weight option, you can control the order in which the votes appear to users. Setting the weight heavier (positive numbers) will make the vote label appear at the bottom of the list. Lighter (a negative number) will push it to the top. To encourage positive voting, a useful order might be higher values, positive votes, at the top, with negative votes at the bottom.

6. Setup the vote/values in the Moderation matrix. Go to "administer >> node moderation" and select the "moderation matrix" tab. On this page you will have to enter the values for the vote labels for each permission role in the vote matrix. The values entered here will be used to create the rating for each node. NOTE: Node ratings are calculated by using the Sum of all votes cast for that node, not an average.  Users with multiple roles are handled so that their votes have a "most value takes precedents" approach.  In other words, when a user with multiple roles casts a vote using the "most" point value among their roles, whether that is a negative or positive number.   

7. Setup initial values for Moderation roles. Next go to "administer >> node moderation" and select the "moderation roles" tab. On this page you will need to enter values for the initial node ratings for each user role.  Depending on how you setup your user roles, an example might be:

    * Administrator 2
    * Trusted User 2
    * Authenticated User 1
    * Anonymous User 0

8. Define types of nodes to be moderated. Go to the "administer >> settings >> node moderation" menu and select which node types will be subject to voting.  NOTE: In order for the "promotion by voting" feature of this module to work for these selected node types, the "promote" option must not be set in Drupal's default workflow.  To check this go to "administer >> content" and look under the "configure >> default workflow" tab.

9. Establish thresholds for voting.  Go to the "administer >> settings >> node moderation" menu and set your desired thresholds for either promoting or unpublishing the node through voting.

10. Set the voting Interval.  Go to the "administer >> settings >> node moderation" menu and enter the number of days that the nodes shall be eligible for voting.

11. Styling the voting form.  You may want to add the optional CSS rule contained in the nmoderation.css file to insure the vote form appears on its own line. For greater control, override the theme_nmoderation_moderation_form() function.

12. Enjoy!


SPONSORS
Brady Jarvis and Alaric Hahn of http://Code0range.net

AUTHOR
Moshe Weitzman <weitzman at tejasa.com>
