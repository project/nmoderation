<?php

/**
 * @file
 * Enables users to vote nodes onto the home page.
*/

define('NMODERATION_VOTE_TYPE', 'option');
define('NMODERATION_CACHE_TYPE', 'points');
define('NMODERATION_TAG', 'vote');

/**
 * Implementation of hook_help().
 */
function nmoderation_help($section = "admin/help#nmoderation") {
  switch ($section) {
    case 'admin/modules#description':
      return t('Enables users to vote nodes onto the home page.');
   }
}

function nmoderation_menu($may_cache) {

$items[] = array(
    'path' => 'admin/settings/nmoderation',
    'title' => t('Nmoderation'),
    'description' => t('Moderate nodes.'),
    'callback' => 'drupal_get_form',
    'callback arguments' => array('nmoderation_admin_settings'),
    'access' => user_access('administer site configuration'),
    'type' => MENU_NORMAL_ITEM, // optional
   );
   
  if ($may_cache) {
    $access = user_access('administer node moderation votes');
    $items[] = array('path' => 'admin/nmoderation', 'title' => t('node moderation'),
      'callback' => 'nmoderation_list_all', 'access' => $access);
    $items[] = array('path' => 'admin/nmoderation/list', 'title' => t('list'),
      'access' => $access, 'type' => MENU_DEFAULT_LOCAL_TASK, 'weight' => -10);

    $access = user_access('administer node moderation configuration');
    $items[] = array('path' => 'admin/nmoderation/matrix', 'title' => t('moderation matrix'),
      'callback' => 'nmoderation_matrix_settings', 'access' => $access, 'type' => MENU_LOCAL_TASK);
    $items[] = array('path' => 'admin/nmoderation/roles', 'title' => t('moderation roles'),
      'callback' => 'nmoderation_role_settings', 'access' => $access, 'type' => MENU_LOCAL_TASK);
    $items[] = array('path' => 'admin/nmoderation/votes', 'title' => t('moderation votes'),
      'callback' => 'nmoderation_vote_settings', 'access' => $access,'type' => MENU_LOCAL_TASK);

    $access = user_access('moderate nodes');
    $items[] = array('path' => 'nmoderation/edit', 'title' => t('edit node moderation vote'),
      'callback' => 'nmoderation_uservote_edit', 'access' => $access, 'type' => MENU_CALLBACK);
    $items[] = array('path' => 'nmoderation/delete', 'title' => t('delete node moderation vote'),
      'callback' => 'nmoderation_uservote_delete', 'access' => $access, 'type' => MENU_CALLBACK);

    $items[] = array('path' => 'nmoderation', 'title' => t('node moderation'),
      'callback' => 'nmoderation_list_unvoted', 'access' => $access);
    $items[] = array('path' => 'nmoderation/voted', 'title' => t('my node moderation votes'),
      'callback' => 'nmoderation_list_voted', 'access' => $access);

    $items[] = array('path' => 'nmoderation/all', 'title' => t('all node moderation votes'),
      'callback' => 'nmoderation_list_all', 'weight' => 5, 'access' => user_access('administer node moderation votes'));
  }
  else {
    if (arg(0) == 'node' && is_numeric(arg(1))) {
    $access = user_access('administer node moderation votes') || user_access('view node moderation votes');
      $items[] = array('path' => 'node/'. arg(1). '/nmoderation', 'title' => t('votes'),
        'type' => MENU_LOCAL_TASK, 'callback' => 'nmoderation_list_node', 'weight' => 5, 'access' => $access, 'callback arguments' => array(arg(1)));
    }
    theme('add_style', drupal_get_path('module','nmoderation') . '/nmoderation.css');
  }
  return $items;
}

/**
 * Implementation of hook_perm().
 */
function nmoderation_perm() {
  return array('nmoderate nodes', 'view node moderation votes', 'administer node moderation votes', 'administer node moderation configuration');
}


/**
 * Validates module dependencies for the module.
 *
 * @param $form The form array passed from hook_form_alter.
 *
 * Set the $module variable to a string which is the name of the module, minus
 * the .module extension.  Set $dependencies to an array of module names which
 * the module is dependent on--each element is a string which is the module name
 * minus the .module extension.  Note that this will not check for any dependencies
 * for the modules this module depends on--only those that are explicitly listed in
 * the $dependencies array.
 */
function nmoderation_system_module_validate(&$form) {
 $module = 'nmoderation';
 $dependencies = array('votingapi');
 foreach ($dependencies as $dependency) {
     if (!in_array($dependency, $form['status']['#default_value'])) {
       $missing_dependency = TRUE;
       $missing_dependency_list[] = $dependency;
     }
 }
 if (in_array($module, $form['status']['#default_value']) && isset($missing_dependency)) {
   db_query("UPDATE {system} SET status = 0 WHERE type = 'module' AND name = '%s'", $module);
   $key = array_search($module, $form['status']['#default_value']);
   unset($form['status']['#default_value'][$key]);
   drupal_set_message(t('The module %module was deactivated--it requires the following disabled/non-existant modules to function properly: %dependencies', array('%module' => $module, '%dependencies' => implode(', ', $missing_dependency_list))), 'error');
 }
}

/* this is a check for module dependencies.  the only way we
   can ensure this check happening when the module is initially
   enabled is to insert the check for when the form is initially
   built, which will also be caught when the admin/module page is
   reloaded upon submission.  this means we never want to call this
   function when the form has been submitted, so make sure there's
   no $_POST. */
function nmoderation_form_alter($form_id, $form) {
  if ($form_id == 'system_modules' && !$_POST) {
    nmoderation_system_module_validate($form);
  }
}

function nmoderation_matrix_settings() {
  $output .= '<h3>Node Moderation vote/value matrix</h3>';
  $output .= drupal_get_form('nmoderation_matrix_settings_form', $form_id);
  return $output;
}

function nmoderation_matrix_settings_form() {
  $result = db_query("SELECT r.rid, r.name FROM {role} r, {permission} p WHERE r.rid = p.rid AND p.perm LIKE '%moderate nodes%'");
  $role_names = array();
  while ($role = db_fetch_object($result)) {
    $role_names[$role->rid] = $role->name;
  }

  $result = db_query('SELECT rid, mid, value FROM {nmoderation_roles} ');
  while ($role = db_fetch_object($result)) {
    $mod_roles[$role->rid][$role->mid] = $role->value;
  }

  $result = db_query('SELECT mid, vote FROM {nmoderation_votes} ORDER BY weight');
  while ($vote = db_fetch_object($result)) {
    $form['votes'][$vote->mid]['vote'] = array(
      '#type' => 'markup',
      '#value' => $vote->vote,
    );
    foreach (array_keys($role_names) as $rid) {
      $form['votes'][$vote->mid][$rid] = array(
        '#type' => 'textfield',
        '#default_value' => $mod_roles[$rid][$vote->mid],
        '#size' => 3,
        '#maxlength' => 4,
      );
    }
  }

  $form['#tree'] = TRUE; // make sure we preserve all hierarchial values.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit votes')
  );

  // Stick a few extra bits into the $form array for the theming function to use
  $form['role_names'] = array(
    '#type' => 'value',
    '#value' => $role_names,
  );

  return $form;
}

function nmoderation_matrix_settings_form_submit($form_id, $form_values) {
  // db_query('DELETE FROM {nmoderation_roles} ');
  foreach (element_children($form_values['votes']) as $mid) {
    foreach (element_children($form_values['votes'][$mid]) as $rid) {
      $value = $form_values['votes'][$mid][$rid];
      $sql = "('$mid', '$rid', '". ($value ? $value : 0) ."')";
      db_query('INSERT INTO {nmoderation_roles} (mid, rid, value) VALUES '. $sql);
    }
  }

  drupal_set_message(t('the vote values have been saved.'));
}

function theme_nmoderation_matrix__settings_form(&$form) {
  $role_names = $form['role_names']['#value'];
  $header = array_merge(array(t('votes')), array_values($role_names));

  foreach (element_children($form['votes']) as $mid) {
    $row = array();
    foreach (element_children($form['votes'][$mid]) as $rid) {
      $row[] = drupal_render($form['votes'][$mid][$rid]);
    }
    $rows[] = $row;
  }

  if (!$rows) {
    $rows[] = array(array('data' => t('No votes have been defined.'), 'colspan' => '5'));
    unset($form['submit']);
  }

  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}

/**
 * Menu callback; allows admin to set default scores for different roles.
 */
function nmoderation_role_settings() {
  $output .= '<h3>Initial node scores</h3>';
  $output .= drupal_get_form('nmoderation_role_settings_form', $form_id);
  return $output;
}

 function nmoderation_role_settings_form() {
  $start_values = variable_get('nmoderation_roles', array());
  
  $result = db_query("SELECT r.rid, r.name FROM {role} r, {permission} p WHERE r.rid = p.rid AND p.perm LIKE '%moderate nodes%' ORDER BY r.rid");
  while ($role = db_fetch_object($result)) {
    $form['role'][$role->rid] = array(
      '#title' => $role->name,
      '#type' => 'textfield',
      '#default_value' => $start_values[$role->rid],
      '#size' => 3,
      '#maxlength' => 4,
    );
  }

  $form['#tree'] = TRUE; // make sure we preserve all hierarchial values.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save scores')
  );

  return $form;
}

function nmoderation_role_settings_form_validate($form_id, $form_values) {
  // make sure everything is numeric/int
}

function nmoderation_role_settings_form_submit($form_id, $form_values) {
    $new_values = array();
    foreach (element_children($form_values['role']) as $rid) {
      $new_values[$rid] = $form_values['role'][$rid]['#value'];
    }
    variable_set('nmoderation_roles', $new_values);
    drupal_set_message(t('the node moderation scores have been saved.'));
}

function theme_nmoderation_role_settings_form(&$form) {
  $header = array(t('user role'), t('initial score'));

  $rows = array();
  foreach (element_children($form['role']) as $rid) {
    $row = array();
    $row[] = $form['role'][$rid]['#title'];
    unset($form['role'][$rid]['#title']);
    $row[] = drupal_render($form['role'][$rid]);
    $rows[] = $row;
  }

  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}

/**
 * Menu callback; displays page for assigning names to vote values.
 */

 function nmoderation_vote_settings() {
  $output .= '<h3>'. t('Node moderation votes overview') .'</h3>';
  $output = drupal_get_form('nmoderation_vote_settings_form', $form_id);
  return $output;
}

 function nmoderation_vote_settings_form() {

  // load up and show any vote types previously defined.
  $result = db_query('SELECT mid, vote, weight FROM {nmoderation_votes} ORDER BY weight');
  while ($vote = db_fetch_object($result)) {
    $form['old'][$vote->mid]['vote'] = array(
      '#type' => 'textfield',
      '#default_value' => $vote->vote,
      '#size' => 32,
      '#maxlength' => 64,
    );
    
    $form['old'][$vote->mid]['weight'] = array(
      '#type' => 'textfield',
      '#default_value' => $vote->weight,
      '#size' => 32,
      '#maxlength' => 64,
    );

    $form['old'][$vote->mid]['delete'] = array(
      '#type' => 'checkbox',
      '#default_value' => 0,
      '#return_value' => 1,
    );
  }
  
  // Add a set of fields for new votes.
  $form['new']['vote'] = array(
   '#type' => 'textfield',
   '#default_value' => $vote->vote,
   '#size' => 32,
   '#maxlength' => 64,
  );
    
  $form['new']['weight'] = array(
    '#type' => 'textfield',
    '#default_value' => $vote->weight,
    '#size' => 32,
    '#maxlength' => 64,
  );

  $form['#tree'] = TRUE; // make sure we preserve all hierarchial values.
  $form[] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  return $form;
}

function nmoderation_vote_settings_form_validate($form_id, $form_values) {
  // make sure $weight is an int and $vote is markup clean, no injections.
}

function nmoderation_vote_settings_form_submit($form_id, $form_values) {
  // loop through the fields for old votes, update or delete as necessary.
  foreach (element_children($form_values['old']) as $mid) {
    if ($form_values['old'][$mid]['delete'] == 1) {
       db_query('DELETE FROM {nmoderation_votes} WHERE mid = %d', $mid);
      db_query('DELETE FROM {nmoderation_roles} WHERE mid = %d', $mid);
    }
    else {
      db_query("UPDATE {nmoderation_votes} SET vote = '%s', weight = %d WHERE mid = %d", $form_values['old'][$mid]['vote'], $form_values['old'][$mid]['weight'], $mid);
    }
  }  
  
  // if a new vote has been added, insert it.
  if ($form_values['new']['vote'] != '') {
    db_query("INSERT INTO {nmoderation_votes} (vote, weight) VALUES ('%s', %d)", $form_values['new']['vote'], $form_values['new']['weight']);
  }

  drupal_set_message(t('the changes have been saved.'));
  drupal_goto('admin/nmoderation/votes'); // do this because we want the form completely rebuilt.
}

function theme_nmoderation_vote_settings_form(&$form) {
  $header = array(t('votes'), t('weight'), t('delete'));
  
  // render the existing-vote fields.
  foreach (element_children($form['old']) as $mid) {
    $row = array();
    $row[] = drupal_render($form['old'][$mid]['vote']);
    $row[] = drupal_render($form['old'][$mid]['weight']);
    $row[] = drupal_render($form['old'][$mid]['delete']);
    $rows[] = $row;
  }

  // render the add-a-vote fields.
  $row = array();
  $row[] = drupal_render($form['new']['vote']);
  $row[] = drupal_render($form['new']['weight']);
  $row[] = '';
  $rows[] = $row;
  
  $output .= theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}
 
function nmoderation_moderation($content_type, $content_id, $vote, $title= NULL) {
  $output = drupal_get_form('nmoderation_moderation_form', $form_id, $content_type, $content_id, $vote, $title= NULL);
  return $output;
}

function nmoderation_moderation_form($content_type, $content_id, $vote, $title= NULL) {
  global $user;
  static $votes;

  if (user_access('moderate nodes')) {
    if (!isset($votes) && $user->roles) {
      $result = db_query('SELECT mid, vote FROM {nmoderation_votes} ORDER BY weight ASC');
      $votes = array();
      while ($dbvote = db_fetch_object($result)) {
        $votes[$dbvote->mid] = $dbvote->vote;
      }
    }
    
    if ($votes) {
      $form['value'] = array(
        '#type' => 'select',
        '#title' => t('Moderate Post'),
        '#default_value' => $vote->value,
        '#options' => $votes,
        '#description' => t('Your vote contributes to whether or not this post appears on the home page.'),
      );
      
      $form['content_type'] = array(
        '#type' => 'hidden',
        '#value' => $content_type,
      );

      $form['content_id'] = array(
        '#type' => 'hidden',
        '#value' => $content_id,
      );

      if ($vote) {
        $form['vote_id'] = array(
          '#type' => 'hidden',
          '#value' => $vote->vote_id,
        );
      }

      if ($title) {
        $form['submit'] = array(
          '#type' => 'submit',
          '#value' => $title,
        );
      }
      return $form;
    }
  }
}

function nmoderation_moderation_form_submit($form_id, $form_values) {
  global $user;
  $content_type = $form_values['content_type'];
  $content_id = $form_values['content_id'];
  $value = $form_values['value'];
  $vote_id = $form_values['vote_id'];

  if ($vote_id) {
    // There's already a vote, we're editing.
    $vote = db_fetch_object(db_query("SELECT * FROM {votingapi_vote} WHERE vote_id=%d", $vote_id));
    if ($vote->uid != $user->uid) {
      $account = user_load(array('uid' => $vote->uid));
    }
    else {
      $account = $user;
    }
  }

  // Use the existing types, or the defaults if there was no original vote.
  $vote->tag = $vote->tag ? $vote->tag : NMODERATION_TAG;
  $vote->value_type = $vote->value_type ? $vote->value_type : NMODERATION_VOTE_TYPE;
  $vote->value = $value;

  votingapi_set_vote($content_type, $content_id, $vote, $account);
  nmoderation_set_promote($content_id, $content_type);

  if ($vote_id) {
    drupal_set_message(t('Vote edited.'));
  }
  else {
    drupal_set_message(t('Vote added.'));
  }
}

function theme_nmoderation_moderation_form($form) {
  // We aren't actually going to do anything fancy, but it's here
  // in case folks want to override it for a given theme.
  $output = drupal_render($form);
  return $output;
}

// if you pass in an $account object, it must have $uid and $roles elements
function nmoderation_get_value($mid, $account = NULL) {
  if (!$account) {
    global $user;
    $account = $user;
  }
  $roles = implode(',', array_keys($account->roles));
  $sql = 'SELECT value FROM {nmoderation_roles} WHERE mid = %d AND rid IN (%s) ORDER BY ABS(value) DESC';
  return db_result(db_query_range($sql, $mid, $roles, 0, 1)); // see comment_moderate() for analogous function
}

function nmoderation_uservote_edit($vote_id) {
  global $user;
  $sql = 'SELECT vote_id, uid, content_id, value FROM {votingapi_vote} WHERE vote_id = %d';
  $vote = db_fetch_object(db_query($sql, $vote_id));

  // may only edit own vote if you are not an admin
  if ($user->uid == $vote->uid || user_access('administer node moderation votes')) {
    $node = node_load($vote->content_id);
    $form = nmoderation_moderation_form('node', $node->nid, $vote, t('Edit vote'));
    
    if ($vote->uid != $user->uid) {
      $form['username'] = array(
        '#type' => 'markup',
        '#title' => t('Voter'),
        '#value' => theme('username', $account),
        '#weight' => -2,
      );
    }
    
    $form['title'] = array(
      '#type' => 'markup',
      '#title' => t('Title'),
      '#value' => $node->title,
      '#weight' => -1,
    );

    //$output .= drupal_get_form('nmoderation_moderation_form');
    $output .= drupal_get_form('nmoderation_moderation_form', 'node', $node->nid, $vote, t('Edit vote'));
    return $output;
  }
  else {
    drupal_access_denied();
  }
}

function nmoderation_uservote_delete($vote_id) {
  $form['vote_id'] = array('#type' => 'value', '#value' => $vote_id);
  $output = confirm_form(
    //'nmoderation_delete_confirm',
    $form,
    t('Are you sure you want to delete this vote?'),
    $_GET['destination'] ? $_GET['destination'] : 'nmoderation/voted',
    t('This action cannot be undone.'),
    t('Delete Vote'),
    t('Cancel') 
  );
  var_export($output);
  return $output;
}

function nmoderation_delete_confirm_submit($form_id, $form_values) {
  global $user;
  if ($form_values['confirm']) {
    $sql = 'SELECT * FROM {votingapi_vote} WHERE vote_id = %d';
    $vote = db_fetch_object(db_query($sql, $vote_id));

    if ($user->uid == $vote->uid || user_access('administer node moderation votes')) {
      votingapi_delete_vote($vote);
      votingapi_recalculate_results($vote->content_type, $vote->content_id);

      nmoderation_set_promote($vote->content_id, $vote->content_type);
      
      drupal_set_message(t('Vote deleted.'));
      drupal_goto('nmoderation/voted');
    }
    else {
      drupal_access_denied();
    }
  }
}

// will either promote or unpublish or do nothing, depending on the curent score for this node
function nmoderation_set_promote($content_id, $content_type = 'node') {
  $result = votingapi_get_voting_result($content_type, $content_id, NMODERATION_CACHE_TYPE, NMODERATION_TAG, 'sum');
  $node = node_load($content_id);
  // have to load terms in order that they don't get lost upon saving the node.

  if ($result->value < variable_get('nmoderation_threshold_unpublish', 4)) {
    $node->status = 0;
    node_save($node);
  }
  elseif ($result->value > variable_get('nmoderation_threshold_promote', 4)) {
    $node->promote = 1;
    node_save($node);
  }
}

function nmoderation_list_unvoted($content_type = 'node') {
  global $user;

  $header = array(
    array('data' => t('title'), 'field' => 'title'),
    array('data' => t('author'), 'field' => 'u.name'),
    array('data' => t('date'), 'field' => 'n.created', 'sort' => 'desc')
  );
  $cols = 2;
  $title = t('my unmoderated nodes');
  $types = implode("','", variable_get('nmoderation_node_types', node_get_types()));
  $sql = "SELECT n.nid, n.created, u.name, n.title, n.uid FROM {node} n INNER JOIN {users} u ON n.uid = u.uid LEFT JOIN {votingapi_vote} v ON n.nid = v.content_id AND v.content_type = '$content_type' AND v.uid = $user->uid WHERE n.type IN ('$types') AND n.status = 1 AND ISNULL(v.vote_id)";
  $min = time()-24*3600*variable_get('nmoderation_vote_interval', 7);
  $sql .= " AND n.created > $min";
  if (!user_access('administer node moderation votes')) {
    $sql .= " AND n.uid != $user->uid";
  }
  $sql .= tablesort_sql($header);
  $result = pager_query(db_rewrite_sql($sql), 50);
  while ($vote = db_fetch_object($result)) {
    $rows[] = array(l($vote->title, "node/$vote->nid"), theme('username', $vote), format_date($vote->created, 'small'));
  }
  if ($pager = theme('pager', NULL, 50, 0)) {
    $rows[] = array(array('data' => $pager, 'colspan' => $cols));
  }
  $output .= theme('table', $header, $rows);
  drupal_set_title($title);
  return $output;
}

// TODO disallow changing of votes on node types which are now unvotable?
function nmoderation_list_voted($content_type = 'node') {
  global $user;
  $header = array(
    array('data' => t('title'), 'field' => 'title'),
    array('data' => t('author'), 'field' => 'u.name'),
    array('data' => t('date'), 'field' => 'n.created', 'sort' => 'desc'),
    array('data' => t('vote')),
    array('data' => t('operations'), 'colspan' => 2)
  );

  $sql = "SELECT n.nid, u.name, n.title, n.changed, n.created, n.uid, nv.vote, v.vote_id FROM {votingapi_vote} v INNER JOIN {nmoderation_votes} nv ON v.value = nv.mid INNER JOIN {node} n ON n.nid = v.content_id INNER JOIN {users} u ON n.uid = u.uid WHERE n.status = 1 AND v.uid = $user->uid AND v.content_type = '$content_type' AND v.value_type = '" . NMODERATION_VOTE_TYPE . "'";
  $sql .= tablesort_sql($header);
  $result = pager_query(db_rewrite_sql($sql), 50);
  while ($vote = db_fetch_object($result)) {
    $row = array(l($vote->title, "node/$vote->nid"), theme('username', $vote), format_date($vote->created, 'small'), $vote->vote);
    if (nmoderation_is_votable($vote)) {
      $row[] = l(t('edit'), "nmoderation/edit/$vote->vote_id");
      $row[] = l(t('delete'), "nmoderation/delete/$vote->vote_id");
    }
    else {
      $row[] = array('data' => t('voting period expired'), 'colspan' => 2);
    }
    $rows[] = $row;
  }
  if ($pager = theme('pager', NULL, 50, 0)) {
    $rows[] = array(array('data' => $pager, 'colspan' => $cols));
  }
  $output .= theme('table', $header, $rows);
  drupal_set_title($title);
  return $output;
}

function nmoderation_list_all($content_type = 'node') {
  $header = array(
    array('data' => t('title'), 'field' => 'title'),
    array('data' => t('date'), 'field' => 'created', 'sort' => 'desc'),
    array('data' => t('voter'), 'field' => 'u.name'),
    array('data' => t('vote'), 'field' => 'nv.vote'),
    array('data' => t('value')),
    array('data' => t('operations'), 'colspan' => 2)
  );
  $sql = "SELECT nv.vote, v.value, v.vote_id, n.nid, n.created, n.title, v.uid, u.name FROM {votingapi_vote} v INNER JOIN {nmoderation_votes} nv ON v.value = nv.mid INNER JOIN {node} n ON v.content_id = n.nid AND v.content_type = '$content_type' AND v.value_type = '". NMODERATION_VOTE_TYPE ."' INNER JOIN {users} u ON v.uid = u.uid WHERE n.status = 1";
  $sql .= tablesort_sql($header);
  $result = pager_query(db_rewrite_sql($sql), 50);
  while ($vote = db_fetch_object($result)) {
    $rows[] = array(
      l($vote->title, "node/$vote->nid"),
      format_date($vote->created, 'small'),
      theme('username', $vote),
      $vote->vote,
      nmoderation_get_value($vote->value, user_load(array('uid'=>$vote->uid))),
      l(t('edit'), "nmoderation/edit/$vote->vote_id"),
      l(t('delete'), "nmoderation/delete/$vote->vote_id")
    );
  }
  if ($pager = theme('pager', NULL, 50, 0)) {
    $rows[] = array(array('data' => $pager, 'colspan' => $cols));
  }
  $output .= theme('table', $header, $rows);
  drupal_set_title($title);
  return $output;
}

function nmoderation_list_node($content_id, $content_type = 'node') {
  if (user_access('administer node moderation votes')) {
    $header = array(
      array('data' => t('voter'), 'field' => 'u.name'),
      array('data' => t('vote'), 'field' => 'v.vote'),
      array('data' => t('value')),
      array('data' => t('operations'), 'colspan' => 2)
    );
    $cols = 4;
  }
  else {
    $header = array(
      array('data' => t('voter'), 'field' => 'u.name'),
      array('data' => t('vote'), 'field' => 'v.vote'),
    );
    $cols = 2;
  }

  $sql = "SELECT nv.vote, v.vote_id, v.uid, v.value, u.name FROM {votingapi_vote} v INNER JOIN {nmoderation_votes} nv ON v.value = nv.mid INNER JOIN {users} u ON v.uid = u.uid WHERE v.content_type = '$content_type' AND v.content_id = %d";
  $sql .= tablesort_sql($header);
  $result = pager_query($sql, 50, NULL, NULL, $content_id);

  if (user_access('administer node moderation votes')) {
    while ($vote = db_fetch_object($result)) {
      $rows[] = array(theme('username', $vote), $vote->vote, nmoderation_get_value($vote->value, user_load(array('uid'=>$vote->uid))), l(t('edit'), "nmoderation/edit/$vote->vote_id"), l(t('delete'), "nmoderation/delete/$vote->vote_id"));
    }
  }
  else {
    while ($vote = db_fetch_object($result)) {
      $rows[] = array(theme('username', $vote), $vote->vote);
    }
  }

  if ($pager = theme('pager', NULL, 50, 0)) {
    $rows[] = array(array('data' => $pager, 'colspan' => $cols));
  }
  $output .= theme('table', $header, $rows);
  $node = node_load($content_id);
  drupal_set_title($title);
  return $output;
}

function nmoderation_admin_settings() {
  $promote = drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 20, 25, 30, 35, 40, 45, 50, 60, 70, 80, 90, 100));
  $unpublish = drupal_map_assoc(array(-1, -2, -3, -4, -5, -6, -7, -8, -8, -10, -11, -12, -13, -14, -15, -20, -25, -30, -100));
  $interval = drupal_map_assoc(range(1,60));

  $form['nmoderation_node_types'] = array(
    '#type' => 'select',
    '#title' => t('Node Types'),
    '#default_value' => variable_get('nmoderation_node_types', node_get_types()),
    '#options' => node_get_types('names'),
    '#description' => t('Select the node types upon which accept node moderation votes.'),
    '#extra' => 0,
    '#multiple' => 1,
  );
  $form['nmoderation_threshold_promote'] = array(
    '#type' => 'select',
    '#title' => t('Promote threshold'),
    '#default_value' => variable_get('nmoderation_threshold_promote', 4),
    '#options' => $promote,
    '#description' => t('When a post exceeds this moderation score, it is immediately <strong>promoted to the front page</strong>.'),
  );
  $form['nmoderation_threshold_unpublish'] = array(
    '#type' => 'select',
    '#title' => t('Unpublish threshold'),
    '#default_value' => variable_get('nmoderation_threshold_unpublish', -2),
    '#options' => $unpublish,
    '#description' => t('When a post drops below this score, it is immediately <strong>unpublished</strong>.'),
  );
  $form['nmoderation_vote_interval'] = array(
    '#type' => 'select',
    '#title' => t('Voting Interval'),
    '#default_value' => variable_get('nmoderation_vote_interval', 7),
    '#options' => $interval,
    '#description' => t('The number of days since the creation of a node when we allow voting.'),
  );

   return system_settings_form($form);
}


function nmoderation_is_votable($node) {
  global $user;
  if (user_access('administer node moderation votes')) {
    return TRUE;
  }
  else {
    $access = user_access('nmoderate nodes') && ($node->uid != $user->uid) && (time()-$node->created < 3600*24*variable_get('nmoderation_vote_interval', 7));
    return $access;
  }
}


function nmoderation_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) {
  switch ($op) {
    case 'view':
        if ($page && nmoderation_is_votable($node) && in_array($node->type, variable_get('nmoderation_node_types', node_get_types()))) {
          global $user;
          $vote = votingapi_get_vote('node', $node->nid, NMODERATION_VOTE_TYPE, NMODERATION_TAG, $user->uid);

          if ($vote->vote_id) {
            $title = t('Edit vote');
          }
          else {
            $title = t('Add vote');
          }
             $node->content['body']['#value'] .= drupal_get_form('nmoderation_moderation_form', 'node', $node->nid, $vote, $title);
        }
        break;
  }
}

function nmoderation_votingapi_calculate(&$cache, $votes, $content_type, $content_id) {
  if ($content_type == 'node' && nmoderation_is_votable(node_load($content_id))) {
    //loop through the $votes collection and calculate the weighted value.
    foreach($votes as $vote) {
      if ($vote->value_type == NMODERATION_VOTE_TYPE && $vote->tag == NMODERATION_TAG) {
        unset($cache[NMODERATION_TAG][NMODERATION_VOTE_TYPE][$vote->value]);

        $account = user_load(array('uid' => $vote->uid));
        $cache[NMODERATION_TAG][NMODERATION_CACHE_TYPE]['sum'] += nmoderation_get_value($vote->value, $account);
      }
    }
  }
}
